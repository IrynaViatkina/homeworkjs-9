const horoscope = [
                ["Козерог", 20],
                ["Водолей", 19],
                ["Рыбы", 20],
                ["Овен", 20],
                ["Телец", 21],
                ["Близнецы", 21],
                ["Рак", 22],
                ["Лев", 21],
                ["Дева", 23],
                ["Весы", 23],
                ["Скорпион", 22],
                ["Стрелец", 22]
];

const yearSign = {
				"17": "Собака",
				"08": "Петух",
				"00": "Обезьяна",
				"92": "Овца",
				"83": "Лошадь",
				"75": "Змея",
				"67": "Дракон",
				"58": "Кролик",
				"50": "Тигр",
				"42": "Бык",
				"33": "Крыса",
				"25": "Свинья"
};

let data = prompt("Please, enter your date of birth: ", "dd.mm.yyyy");
let date = new Date(data.split(".").reverse().join(","));

alert(`You are ${getAge(date)} years old.`);
alert(`Your zodiac sign is ${getZodiac(date)}.`);
alert(`Your year sign is ${getYearSign(date)}.`);

function getAge(date){
	let today = new Date();
	let age = today.getFullYear() - date.getFullYear();
	return (date.getMonth() <= today.getMonth() && date.getDate() <= today.getDate()) ?  age : age - 1;
}

function getZodiac(date){
	let month = date.getMonth();
    let day = date.getDate();
	return horoscope[month][1] >= day ? horoscope[month][0] : horoscope[month === 11 ? 0 : month + 1][0];
}

function getYearSign(date){
	let sign = (date.getFullYear() / 12).toFixed(2).split(".");
	return yearSign[sign[1]];
}